# Windows Instance Standard 

## Editor

Windows is the editor of this metric.

## Description 

This metric is relatively simple because it proposes to license one or more instances of the Windows products.

## Formula description

The number of required licenses shall be determined by counting the number of RedHat products installation.

## Formula 

∑(P(numberofcores))

Where P is each processor on which the database CAN run.

Minimum 8 cores/processor, 16 cores/server. Every cores must be covered by a license.

<span style="color:red">**Important : licenses are sold by 2**</span>

## Further details

You can find further details about this metric on the Windows website [here](http://download.microsoft.com/download/1/E/0/1E056F4E-3126-48AC-8ECD-AF41F7FFAC0C/Hybrid_Cloud_Windows_Server_2016_Licensing_Datasheet_EN-IN.pdf)
