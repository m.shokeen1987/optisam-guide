# RedHat Instance Standard

## Editor

RedHat is the editor of this metric.

## Description 

This metric is relatively simple because it proposes to license one or more instances of the RedHat products.

## Formula description

The number of required licenses shall be determined by counting the number of RedHat products installation.

## Formula 

∑(numberOfRedHatProductsInstalled)
