<link rel="stylesheet" href="../../../css/enlargeImage.css" />

# Users management

<span style="color:red">First, make sure that you are an admin</span>

## What is a user ?

User is anybody who can access/use OpTISAM application. User can be human or api consumers. Each user has a role and a set of groups to which he belongs.

## Access

You can access to "Users Management" by clicking here :

![select APM](../../img/userMana/accessu.jpg){: .zoom}

## Presentation

The presentation table is : 

![select APM](../../img/userMana/presentation.jpg){: .zoom}

You can see :  
- Last Name : The last name of the user  
- First Name : The first name of the user  
- Email : The email of the user  
- Groups : The groups to which the user belongs  
- Role : The role of the user (USER or ADMIN)  

## Possibilities

You can do 3 things from there :  
- <span style="color:red">Add a new user</span> ([here](#add-a-new-user))  
- <span style="color:blue">Edit an existing user</span> ([here](#edit-an-existing-user))  
- <span style="color:green">Delete an existing user</span> ([here](#delete-an-existing-user))  

![select APM](../../img/userMana/possibilities.jpg){: .zoom}

## Add a new user

You have to click on "Add User" : 

![select APM](../../img/userMana/add1.jpg){: .zoom}

This screen will be shown : 

![select APM](../../img/userMana/add2.jpg){: .zoom}

You will have to fill in all these fields :  
- Name : The first name of the user  
- Surname : The last name of the user  
- Email : The email of the user  
- Group : Select the group(s) to which the user will belong  
- Role : Select the role of the user  

Once, you have filled in all the fields, you have to click on "Create" to create the new user.  
The default password is "password" when you create a new user.  
You will have to tell the user that his account has been created as no e-mail will be sent to notify him.

## Edit an existing user

You have to click on the pen in the line of the user you want to edit : 

![select APM](../../img/userMana/edit1.jpg){: .zoom}

This screen will be shown : 

![select APM](../../img/userMana/edit2.jpg){: .zoom}

You will be able to edit the role of the user, either "ADMIN" or "USER". Then you have to click on "Update" to edit the user.

## Delete an existing user

You have to click on the trash in the line of the user you want to edit : 

![select APM](../../img/userMana/delete1.jpg){: .zoom}

This window will be shown : 

![select APM](../../img/userMana/delete2.jpg){: .zoom}

You just have to click on "OK" to delete the user.

<script src="../../../js/zoomImage.js"></script>