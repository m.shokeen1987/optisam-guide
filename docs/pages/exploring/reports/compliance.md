<link rel="stylesheet" href="../../../../css/enlargeImage.css" />

# Reports - Compliance

**This reports section allows you to create reports in CSV or PDF for the compliance of your products.**

## Access to the page

Click on "Reports" : 

![select APM](../../../img/exploring/reports/complianceAccess.jpg){: .zoom}

## Create the report : 

Click on "Create Report" : 

![select APM](../../../img/exploring/reports/complianceFirst.jpg){: .zoom}

Select :  
- Report Type : Select "Compliance"  
- Editor : The editor on which you want to create the report  
- Product : The product(s) on which you want to create the report  

![select APM](../../../img/exploring/reports/complianceSecond.jpg){: .zoom}

Click on "Create" to create your report.

## Results of the report

You can find the report you've just created in the list.  
To see the results of a report, click on "Export" and select "PDF" or "CSV". This will generate a file that you can download.

![select APM](../../../img/exploring/reports/complianceThird.jpg){: .zoom}

### CSV

In the CSV report you will find a table with all the attributes to know if your product is compliant : 

![select APM](../../../img/exploring/reports/complianceCSV.jpg){: .zoom}

The attributes are :  
- SKU : Identifier of the rights you have acquired  
- SWIDTag : Identifier of the product  
- Editor : Editor of the product  
- Metric : Identifier of the metric used for this product  
- ComputedLicenses : Number of licenses needed for your deployment  
- AcquiredLicenses : Number of licenses acquired  
- Delta (number) : Difference between "Acquired" and "Computed" licenses (positive = underusage, negative = counterfeiting)  
- Delta (euros) : Difference between "Acquired" and "Computed" licenses in euros (positive = underusage, negative = counterfeiting)  
- TotalCost : Total cost of your "Acquired licenses"  
- AvgUnitPrice : The unit price of a license  

### PDF

In the PDF report you will find a table with all the attributes to know if your product is compliant : 

![select APM](../../../img/exploring/reports/compliancePDF.jpg){: .zoom}

The attributes are the same as in the CSV file.

<script src="../../../../js/zoomImage.js"></script>