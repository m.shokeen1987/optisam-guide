# Global presentation

To compute a compliance, OpTISAM needs to know on which **equipments** your **software products** are installed. The goal of the inventory is to reference all of your **products** and the **equipments** on which they are installed.  
OpTISAM's inventory structure must reflect the structure of your IT park.  

So, the inventory of OpTISAM is originally designed to have associations between **"Products"** and **"Equipments"**.
We have added the possibility to have **"Applications"** and associations between the **"Applications"** and the **"Products"** in order to allow users to also have an **"Applications"** view. But this is not the first aim of OpTISAM, so the **"Applications"** are not mandatory to have a good use of OpTISAM.


OpTISAM inventory contains :  
- **[Equipments](../equipments)** (Mandatory)  
- **[Products](../products)** (Mandatory)  
- **[Applications](../applications)** (Optionnal)  