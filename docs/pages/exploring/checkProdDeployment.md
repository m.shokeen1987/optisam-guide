<link rel="stylesheet" href="../../../css/enlargeImage.css" />

# Check products deployment

## Access to the page

You have to click on "Dashboard" and "Quality" : 

![select APM](../../img/exploring/checkProdDeployment1.jpg){: .zoom}

## Check the products that are not deployed

In order to know, which products are not deployed in your inventory (and for which you provided acquired rights to OpTISAM), click on the number here :  

![select APM](../../img/exploring/checkProdDeployment2.jpg){: .zoom}

This screen will be shown : 

![select APM](../../img/exploring/checkProdDeployment3.jpg){: .zoom}

## Check the products that are not licensed

In order to know, which products are not licensed (and are deployed on some equipments), click on the number here :  

![select APM](../../img/exploring/checkProdDeployment4.jpg){: .zoom}

This screen will be shown : 

![select APM](../../img/exploring/checkProdDeployment5.jpg){: .zoom}

<script src="../../../js/zoomImage.js"></script>