<link rel="stylesheet" href="../../../../css/enlargeImage.css" />

# Aggregations

An aggregation is something that you can create in order to regroup some products (e.g : by editor or metric).

## Access to the page

Click on "Acquired Rights" and on "Aggregations" : 

![select APM](../../../img/exploring/licensing/aggregationsu.jpg){: .zoom}

## Description

You are now able to see the list of all of your acquired rights by aggregations :

![select APM](../../../img/exploring/licensing/aggregationsListu.jpg){: .zoom}

You have two tables in one, the first one is for your aggregations, the attributes are :  
- SKU : The identifier of your acquired right  
- SWIDtag : The identifier of the product. 
- Aggregation name : The name of the aggregation  
- Editor : The editor of the products in the aggregation  
- Metric : Name of the metric used for the acquired rights of the aggregation  
- Total cost : The total cost (license + maintenance) for all the products included in the aggregation   

The second one is giving the details of the rights for each product present in the aggregations :  
- SKU : The identifier of your acquired right  
- SWIDtag : The identifier of the product  
- Product name : The name of the product (You can click on it to have more information about it)  
- Version / Editor : The version and the editor of the product  
- Metric : Name of the metric used for the acquired right  
- Acquired licenses : The number of licences acquired  
- Licenses under maintenance number : The number of licenses under maintenance acquired  
- Start / End of the maintenance : Dates of start and end of the maintenance  
- Licenses under maintenance : Is your license under maintenance  
- AVG License / Maintenance unit price : Price for each unit of license or maintenance acquired  
- Total purchase / maintenance cost : The total cost of your license / maintenance acquired  
- Total cost : The total cost (license + maintenance) for the product   

## How to create an aggregation

If you're an admin and you want to know how to create an aggregation, just click [here](../../../managing/aggregationsManagement).

<script src="../../../../js/zoomImage.js"></script>