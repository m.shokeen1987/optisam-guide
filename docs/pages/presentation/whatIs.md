# What is OpTISAM ?

OpTISAM is an E2E Software Asset Lifecycle Management to manage Legal and Finacial Risk associated with software usage.

It enables you to :

- Manage the software licences of your IT park :  
	- By exploring your equipment park inventory and what is installed on it  
	- By exploring all of your software products and applications  
	- By exploring the acquired licenses of your products  
- Check the compliance of your IT park regarding software licenses  
	- Different metrics are included (including Oracle & IBM)  
- Simulate compliance impacts of a change in your park or in the license  
- Create license compliance reports of your IT park  
- Manage the access with users and groups  
- Isolate the data at a scope level  

## Who can use OpTISAM ?

OpTISAM is a tool for Software Asset Managers.