# OpTISAM IT Documentation

This documentation is designed to help you to use OpTISAM.  
It is divided in 2 sections : 

1. The first is designed to help you if you are using OpTISAM as a **user** and is made of :
	1. [Introduction](pages/presentation/whatIs)
	2. [Features](pages/exploring/parkInventory/presentation)
2. The second is designed to help you if you are using OpTISAM as an **admin** and is made of :
	1. [Configure OpTISAM](pages/configure/createEquipmentTypes)
	2. [Administration](pages/managing/equipmentsManagement)

You can also find a [glossary](pages/glossary) if there are words that you don't understand.

There is a section dedicated to the computation of the metrics called [SAM](pages/sam/ibm).

You can search for any specific terms in the search bar on the top right of the screen.  

If you don't find the answer(s) you're looking for in this documentation, please check the "[Contact](pages/contact)" section.